#!/bin/bash
RCON_PASSWD="Patate"
IOQ3_SERVER_IP="192.168.1.189"
IOQ3_SERVER_PORT=27960
LOOP_DELAY=1
CMD_DELAY=2
STATUS_TEMP_FILE=/tmp/ioq3-status.txt
VAR_TEMP_FILE=/tmp/ioq3-temp.txt
BEST_PLAYER=""
HIGH_SCORE=""
MAP_ARRAY=(q3dm0 q3dm1 q3dm2 q3dm3 q3dm4 q3dm5 q3dm6 q3dm7 q3dm8 q3dm9)
CURRENT_MAP_POS=0
rm -f $STATUS_TEMP_FILE
rm -f $VAR_TEMP_FILE
#echo $'\377'$'\377'$'\377'$'\377'"rcon Patate map q3dm3" | nc -uw1 127.0.0.1 27960
while true
do
	#echo "Getting Status"
	#echo "Printing Status"
	#printf '\xFF\xFF\xFF\xFFrcon '"$RCON_PASSWD"''" status "' \n' | nc -u -n -w 1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT > $STATUS_TEMP_FILE
	echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD status" | nc -uw1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT > $STATUS_TEMP_FILE
	cat $STATUS_TEMP_FILE
	if [ -s $STATUS_TEMP_FILE ]
	then
		#echo "Status file STATUS_TEMP_FILE not empty"
		HIGH_SCORE="$(egrep -v 'print|num|map|---' $STATUS_TEMP_FILE | sort -nrk 2 | head -1 | awk '{print $2}')"
		BEST_PLAYER="$(egrep -v 'print|num|map|---' $STATUS_TEMP_FILE | sort -nrk 2 | head -1 | awk '{print $4}')"
	fi
	#printf '\xFF\xFF\xFF\xFFrcon '"$RCON_PASSWD"''" fraglimit "' \n' | nc -u -n -w 1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT > $VAR_TEMP_FILE
	echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD fraglimit" | nc -uw1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT > $VAR_TEMP_FILE
	#echo "Getting Fraglimit"
	if [ -s $VAR_TEMP_FILE ]
	then
		FRAGLIMIT=$(egrep -v "print" $VAR_TEMP_FILE | cut -d '^' -f 1 | cut -d '"' -f 4)
	fi
	#Check if Variables are not epmty
	if [ -n $HIGH_SCORE ] && [ -n $FRAGLIMIT ] && [ "$HIGH_SCORE" != "" ] && [ "$FRAGLIMIT" != "" ]
	then
		if [ $HIGH_SCORE == $FRAGLIMIT ]
			then
			echo "END OF GAME - Winner is $BEST_PLAYER with $HIGH_SCORE"
			echo "END OF GAME - Winner is $BEST_PLAYER with $HIGH_SCORE">> $STATUS_TEMP_FILE
			cp $STATUS_TEMP_FILE $STATUS_TEMP_FILE-$(date +%Y-%m-%d-%H-%M)
			BEST_PLAYER=""
			HIGH_SCORE=""
			echo "Switching Map in 3 seconds"
			sleep 3
			echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD Switching Map in 5 seconds" | nc -uw1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT
			#printf '\xFF\xFF\xFF\xFFrcon '"$RCON_PASSWD"''" map q3dm0 "' \n' | nc -u -n -w 1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT
			echo "Switching map to ${MAP_ARRAY[$CURRENT_MAP_POS]}"
			#echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD map ${MAP_ARRAY[$CURRENT_MAP_POS]}" | nc -uw1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT
			#echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD map ${MAP_ARRAY[$CURRENT_MAP_POS]}"  nc -uw5 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT
			echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD map ${MAP_ARRAY[$CURRENT_MAP_POS]}" | nc -uw5 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT
			#echo $'\377'$'\377'$'\377'$'\377'"rcon $RCON_PASSWD map q3dm2" | nc -uw1 $IOQ3_SERVER_IP $IOQ3_SERVER_PORT
			let CURRENT_MAP_POS++
			if [ $CURRENT_MAP_POS == $(( ${#MAP_ARRAY[@]} - 1 )) ]
			then
				CURRENT_MAP_POS=0
			fi
		else
			echo BEST PLAYER IS $BEST_PLAYER WITH SCORE $HIGH_SCORE - FRAGLIMIT IS $FRAGLIMIT
		fi
	sleep $LOOP_DELAY
	#else
	#	echo No data
	fi
	#sleep $LOOP_DELAY
done
